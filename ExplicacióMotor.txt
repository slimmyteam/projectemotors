1. El primer que hem fet és crear una classe Singleton ("KojiMotor") que s'encarrega d'administrar el bucle principal del motor.
La classe està formada per les funcions d'init i update (també tenim la de exit però no la fem servir) que s'encarreguen d'establir el deltaTime amb un bucle i cridar als mètodes dels diferents objectes que tenen les llistes del motor.

El motor té 3 llistes diferents que controlaran l'estat dels objectes a l'execució: "gameobjectsInGame","gameobjectsToAdd","gameobjectsToRemove". També té mètodes per afegir objectes a aquestes llistes i un per agafar el deltaTime.

- Init:
Activem la variable "isRunning" a true per fer un bucle i creem les variables de "currentTime" i "lastTime" amb les quals calcularem la diferència de temps entre les iteracions del bucle tenint en compte que el temps agafat es troba en nanosegons. Entre cada iteració del bucle esperem un mil·lisegon.

-Update:
A l'update primer afegim tots els gameobjects en la llista del ToAdd i realitzem el seu start, un cop afegits netegem la llista del ToAdd i cridem l'update de tots els objectes a la llista de gameobjectsInGame. A continuació, recorrem la llista de gameobjectsToRemove i els traiem de la llista de InGame i cridem al seu mètode "onDestroy". Un cop finalitzat esborrem la llista de ToRemove.


2. Hem creat una clase final "GameObject", és final perquè no volem que es modifiqui. Aquesta classe conté tres llistes: una que servirà per gestionar els components que té el GameObject, una altra que contindrà els components que s'han d'afegir al GameObject i una última que contindrà els components que s'han d'eliminar del GameObject.

De la mateixa manera que fa el "KojiMotor", la classe "GameObject" també disposa dels mètodes start(), update() i onDestroy(), on gestiona les tres llistes mencionades anteriorment i conformen el seu cicle de vida.

3. Com a components, tenim les classes: Spawner, Personatge (les classes Jugador i Enemic hereten de Personatge). Els components  disposen dels mètodes start(), update() i destroy(), que hereten de la classe abstracta "Component". També tenen com a atribut el GameObject que els conté.

En concret, la classe Spawner, utilitza una factoria per crear els diferents tipus de GameObject: Jugador i Enemic. Concretament, la factoria crea un GameObject i, a partir del tipus de component que li passem per paràmetre, li afegeix el component al GameObject i el retorna. 

En relació a les classes Jugador i Enemic, són components que contenen les característiques/stats que s'afegeixen al GameObject (nom, vida, temps).

4. Finalment, disposem d'un Main que crea els GameObjects i la factoria.


RESULTAT OBTINGUT:
 ---- START Spawner ----
Creo l'spawner de: Kojima GO
Creo l'spawner de: Mario Bros GO
Creo l'spawner de: Slime Tendero GO
Spawneo un: Kojima
(!!!) Kaboom el spawner de: Kojima GO (!!!)
 ---- START Kojima ----
Spawneo un: Slime Tendero
Kojima pierde vida y ahora tiene 5
 ---- START Slime Tendero ----
Spawneo un: Mario Bros
Kojima pierde vida y ahora tiene 4
 ---- START Mario Bros ----
Spawneo un: Slime Tendero
Kojima pierde vida y ahora tiene 3
 ---- START Slime Tendero ----
Slime Tendero pierde vida y ahora tiene 2
Mario Bros pierde vida y ahora tiene 8
Kojima pierde vida y ahora tiene 2
Mario Bros pierde vida y ahora tiene 7
Spawneo un: Mario Bros
Spawneo un: Slime Tendero
(!!!) Kaboom el spawner de: Mario Bros GO (!!!)
 ---- START Mario Bros ----
 ---- START Slime Tendero ----
Kojima pierde vida y ahora tiene 1
>>>> Ha muerto: Kojima
Slime Tendero pierde vida y ahora tiene 1
>>>> Ha muerto: Slime Tendero
Mario Bros pierde vida y ahora tiene 6
Slime Tendero pierde vida y ahora tiene 2
.........Destruyendo Kojima.........
.........Destruyendo Slime Tendero.........
Mario Bros pierde vida y ahora tiene 5
Mario Bros pierde vida y ahora tiene 8
Spawneo un: Slime Tendero
(!!!) Kaboom el spawner de: Slime Tendero GO (!!!)
Mario Bros pierde vida y ahora tiene 7
Slime Tendero pierde vida y ahora tiene 2
 ---- START Slime Tendero ----
Mario Bros pierde vida y ahora tiene 4
Slime Tendero pierde vida y ahora tiene 1
>>>> Ha muerto: Slime Tendero
.........Destruyendo Slime Tendero.........
Mario Bros pierde vida y ahora tiene 6
Mario Bros pierde vida y ahora tiene 3
Mario Bros pierde vida y ahora tiene 2
Mario Bros pierde vida y ahora tiene 5
Slime Tendero pierde vida y ahora tiene 1
>>>> Ha muerto: Slime Tendero
Slime Tendero pierde vida y ahora tiene 2
.........Destruyendo Slime Tendero.........
Mario Bros pierde vida y ahora tiene 1
>>>> Ha muerto: Mario Bros
Mario Bros pierde vida y ahora tiene 4
.........Destruyendo Mario Bros.........
Mario Bros pierde vida y ahora tiene 3
Slime Tendero pierde vida y ahora tiene 1
>>>> Ha muerto: Slime Tendero
.........Destruyendo Slime Tendero.........
Mario Bros pierde vida y ahora tiene 2
Mario Bros pierde vida y ahora tiene 1
>>>> Ha muerto: Mario Bros
.........Destruyendo Mario Bros.........


