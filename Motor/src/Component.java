public abstract class Component {
    private GameObject m_GameObject;

    public GameObject getGameObject(){
        return m_GameObject;
    }

    public void setGameObject(GameObject gameObject) {
        this.m_GameObject = gameObject;
    }
    public abstract void start();
    public abstract void update();
    public abstract void destroy();

}
