public class GameObjectFactory {
    public static GameObject create(String tipus, String nom, int vida, double temps){
        GameObject go = new GameObject(nom);
        switch (tipus) {
            case "Jugador":
                go.addComponent(new Jugador(nom, vida, temps));
                break;
            case "Enemic":
                go.addComponent(new Enemic(nom, vida, temps));
                break;
            default:
                System.out.println("He entrado en default");
                break;
        }

        return go;
    }
}
