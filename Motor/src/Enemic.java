public class Enemic extends Personatge{
    public Enemic(String nom, int vida, double temps) {
        super(nom, vida, temps);
    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        tempsTranscorregut += KojiMotor.getInstance().getDeltaTime();
        if (tempsTranscorregut >= temps) {
            System.out.println(nom+" pierde vida y ahora tiene "+vida);
            this.comprobarMuerto();
            tempsTranscorregut -= temps;
        }
    }

    @Override
    public void destroy() {
    }
}
