import java.awt.geom.Point2D;

public abstract class Personatge extends Component{
    protected String nom;
    protected int vida;
    protected double temps;
    protected double tempsTranscorregut;
    protected double velocitat;
    protected Point2D posicio;

    public Personatge(String nom, int vida, double temps) {
        this.nom = nom;
        this.vida = vida;
        this.tempsTranscorregut = 0;
        this.temps = temps;
    }

    public void comprobarMuerto(){
        this.vida--;

        if (vida <= 0) {
            KojiMotor.getInstance().removeGameObject(this.getGameObject());
            System.out.println(">>>> Ha muerto: "+nom);
            }
    }

}
