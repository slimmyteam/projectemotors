import java.util.ArrayList;

public final class GameObject { // Es final porque nunca vamos a modificar el GameObject en sí, sino sus componentes

    public String nom;
    private ArrayList<Component> m_ComponentsInGame = new ArrayList<>();
    private ArrayList<Component> m_ComponentsToAdd = new ArrayList<>();
    private ArrayList<Component> m_ComponentsToRemove = new ArrayList<>();

    public GameObject(String nom){
        this.nom = nom;
    }

    public GameObject(String nom, ArrayList<Component> componentsToAdd){
        this.nom = nom;
        m_ComponentsToAdd = componentsToAdd;
    }
    public void start() {
        System.out.println(" ---- START "+nom+" ----");
        for(Component component : m_ComponentsInGame){
            component.start();
        }
    }
    public void update() {
        // Primer afegim tots els components en la llista del ToAdd i realitzem el seu start
        for(Component component : m_ComponentsToAdd){
            m_ComponentsInGame.add(component);
            component.start();
        }
        // Netegem la llista dels components ToAdd
        m_ComponentsToAdd.clear();

        // Crida l'update de tots els components
        for(Component component : m_ComponentsInGame){
            component.update();
        }

        // Crida el destroy de tots els components de la llista de components ToRemove
        for (Component component : m_ComponentsToRemove){
            m_ComponentsInGame.remove(component);
            component.destroy();
        }
        // Netegem la llista dels components ToRemove
        m_ComponentsToRemove.clear();
    }
    public void onDestroy() {
        for(Component component : m_ComponentsInGame){
            component.destroy();
        }
        m_ComponentsInGame.clear();
        System.out.println(".........Destruyendo "+nom+".........");
    }

    public void addComponent(Component component){
        m_ComponentsToAdd.add(component);
        component.setGameObject(this);
    }

    public void removeComponent(Component component){
        m_ComponentsToRemove.add(component);
    }

    public boolean hasComponent(Component component){
        return this.m_ComponentsInGame.contains(component);
    }

    @Override
    public String toString() {
        return "GameObject{" +
                "nom='" + nom + '\'' +
                '}';
    }

    public ArrayList<Component> getComponentsToAdd() {
        return m_ComponentsToAdd;
    }

    public ArrayList<Component> getComponentsInGame() {
        return m_ComponentsInGame;
    }

}
