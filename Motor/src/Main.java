//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        KojiMotor motor = KojiMotor.getInstance();

        //Creem els gameobjects i els hi posem el component 'Jugador'
        GameObject kojimaGO = new GameObject("Kojima GO");
        kojimaGO.addComponent(new Jugador("Kojima", 5, 1));
        GameObject marioBrosGO = new GameObject("Mario Bros GO");
        marioBrosGO.addComponent(new Jugador("Mario Bros", 8, 1));
        GameObject slimeTenderoGO = new GameObject("Slime Tendero GO");
        slimeTenderoGO.addComponent(new Jugador("Slime Tendero", 2, 2));
        GameObject ashGO = new GameObject("Ash GO");
        ashGO.addComponent(new Jugador("Ash", 5, 2));

        //Creem els gameobjects i els hi posem el component 'Enemic'
        GameObject konamiGO = new GameObject("Konami GO");
        konamiGO.addComponent(new Enemic("Konami", 3, 4));
        GameObject bowserGO = new GameObject("Bowser GO");
        bowserGO.addComponent(new Enemic("Bowser", 7, 1));
        GameObject slimeGoldoGO = new GameObject("Slime Goldo GO");
        slimeGoldoGO.addComponent(new Enemic("Slime Goldo", 5, 2));
        GameObject teamRocketGO = new GameObject("Team Rocket GO");
        teamRocketGO.addComponent(new Enemic("Team Rocket", 10, 2));

        //Incorporem el component 'Spawner' al gameobject spawnerGO
        GameObjectFactory factory = new GameObjectFactory();
        GameObject spawnerGO = new GameObject("Spawner");
        spawnerGO.addComponent(new Spawner(kojimaGO,1,1, factory));
        spawnerGO.addComponent(new Spawner(marioBrosGO,2,3, factory));
        spawnerGO.addComponent(new Spawner(slimeTenderoGO,4,2, factory));
        spawnerGO.addComponent(new Spawner(ashGO,1,5, factory));
        spawnerGO.addComponent(new Spawner(konamiGO,2,5, factory));
        spawnerGO.addComponent(new Spawner(bowserGO,4,5, factory));
        spawnerGO.addComponent(new Spawner(slimeGoldoGO,1,5, factory));
        spawnerGO.addComponent(new Spawner(teamRocketGO,3,5, factory));

        motor.addGameObject(spawnerGO);

        motor.init();
    }

}