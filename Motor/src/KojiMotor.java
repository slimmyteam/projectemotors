import java.util.ArrayList;

public class KojiMotor {
    private static KojiMotor motorInstance;
    ArrayList<GameObject> gameObjectsInGame = new ArrayList<>();
    ArrayList<GameObject> gameObjectsToAdd = new ArrayList<>();
    ArrayList<GameObject> gameObjectsToRemove = new ArrayList<>();

    private boolean isRunning = false;
    private double deltaTime = 0;

    private KojiMotor() { // CONSTRUCTOR
    }

    public static KojiMotor getInstance() { // SINGLETON
        if (motorInstance == null) {
            motorInstance = new KojiMotor();
        }
        return motorInstance;
    }


    public void init(){
        isRunning = true;
        double currentTime;
        double lastTime = System.nanoTime();
        while (isRunning){
            currentTime = System.nanoTime();
            deltaTime = (currentTime - lastTime)/1000000000d;
            lastTime = currentTime;

            this.update();

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void update(){
        // Primer afegim tots els gameobjects en la llista del ToAdd i realitzem el seu start
        for(GameObject gameObject : gameObjectsToAdd){
            gameObjectsInGame.add(gameObject);
            gameObject.start();
        }
        // Netegem la llista dels gameobjects ToAdd
        gameObjectsToAdd.clear();

        // Crida l'update de tots els GameObjects
        for(GameObject gameObject : gameObjectsInGame){
            gameObject.update();
        }

        // Crida el destroy de tots els gameobjects de la llista de gameobject ToRemove
        for (GameObject gameObject : gameObjectsToRemove){
            gameObjectsInGame.remove(gameObject);
            gameObject.onDestroy();
        }
        // Netegem la llista dels gameobjects ToRemove
        gameObjectsToRemove.clear();
    }

    public void addGameObject(GameObject gameObject){
        gameObjectsToAdd.add(gameObject);
    }

    public void removeGameObject(GameObject gameObject){
        gameObjectsToRemove.add(gameObject);
    }

    public double getDeltaTime() {
        return deltaTime;
    }

    public void exit(){
        isRunning = false;
    }

}
