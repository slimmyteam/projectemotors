public class Spawner extends Component{

    public String nom;
    private GameObject gameObject;
    private int quantitat;
    private  double tempsDeCreacio;
    private double tempsTranscorregut;
    private GameObjectFactory factory;

    public Spawner(GameObject _gameObject, int _quantitat, double _tempsDeCreacio, GameObjectFactory _factory)
    {
        nom = _gameObject.nom;
        gameObject = _gameObject;
        quantitat = _quantitat;
        tempsDeCreacio = _tempsDeCreacio;
        tempsTranscorregut = 0;
        factory = _factory;

    }

    @Override
    public void start() {
        System.out.println("Creo l'spawner de: "+nom);
    }

    private void spawn(){
        GameObject go = null;
        for(Component componente : gameObject.getComponentsToAdd()){
            if(componente instanceof Personatge){
                go = factory.create(componente.getClass().getSimpleName(),((Personatge) componente).nom,((Personatge) componente).vida,((Personatge) componente).temps);
            }
        }

        KojiMotor.getInstance().addGameObject(go);
        System.out.println("Spawneo un: "+go.nom);
    }

    @Override
    public void update() {
        tempsTranscorregut += KojiMotor.getInstance().getDeltaTime();
        if (tempsTranscorregut >= tempsDeCreacio){
            spawn();
            quantitat--;
            if(quantitat <= 0) {
                getGameObject().removeComponent(this);
            }
            tempsTranscorregut -= tempsDeCreacio;
        }
    }

    @Override
    public void destroy() {
        System.out.println("(!!!) Kaboom el spawner de: "+nom+" (!!!)");
    }
}
