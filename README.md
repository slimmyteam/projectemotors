# ⚙️ Motor de videojuegos ⚙️
Desarrollo de motor básico basado en Unity (que es el motor que hemos utilizado durante los dos años del grado superior).

## 📄 Descripción
Proyecto realizado para la asignatura de "Videojuegos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white)
- Patrón de diseño Singleton.

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el siguiente documento: [Explicación motor](https://gitlab.com/slimmyteam/projectemotors/-/blob/main/Explicaci%C3%B3Motor.txt?ref_type=heads).

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.
